from matplotlib import pyplot as plt
import numpy as np
import math

theta   = np.array([0.88642008,  3.55712223,  2.09602986,  2.60555411,  1.72797397,
        3.05756992,  3.83869533,  3.20995575,  2.37156094,  1.23950927,
        3.23243006,  4.34179476,  4.09987265,  3.27137452,  2.63958147,
        3.08883046,  2.71876263,  2.22629754,  1.4303203 ,  3.62325709])[1::2]
theta = np.round(sorted(theta),1)
print theta
n_ocurrs = np.array([0 for t in theta])
theta_mean = theta.mean()

####################################################################################
# As numbers
####################################################################################
print "theta_mean %.5f [rad]" %theta_mean
print "theta_mean %.5f [deg]" %(theta_mean*180/np.pi)

# Figure 1
plt.figure(figsize=(8,2))
plt.plot(theta, n_ocurrs, 'go', alpha=0.75)
plt.plot([theta_mean, theta_mean], [-1,1], '-g', lw=2.0)
#plt.xlabel("t [?]")
plt.gca().set_yticks([])
plt.savefig("1plot1D.png", bbox_inches='tight')
plt.close()

# Figura 2
#plt.figure(figsize=(6,4))
plt.hist(theta, color='g', alpha=0.5)
plt.plot([theta_mean, theta_mean], [0,5], 'r', lw=2.0)
#plt.xlabel("t [?]")
#plt.ylabel("Ocurrencias")
#plt.suptitle("Histograma")
plt.savefig("1histo.png", bbox_inches='tight')
plt.close()

####################################################################################
# As angle
####################################################################################
cos_mean = np.cos(theta).mean()
sin_mean = np.sin(theta).mean()
r = (cos_mean**2 + sin_mean**2)**.5
trig_mean = math.atan2(sin_mean, cos_mean)
print cos_mean, sin_mean
print "trig_mean %.5f [rad]" %trig_mean
print "trig_mean %.5f [deg]" %(trig_mean*180/np.pi)

# Figure 3
plt.figure(figsize=(8,2))
plt.plot(theta, n_ocurrs, 'rs', alpha=0.5)
ax=plt.gca()
ax.set_xticks([0, np.pi, 2*np.pi])
ax.set_xticklabels([r"0", r"$\pi$", r"$2 \pi$"])
ax.set_yticks([])
plt.xlabel("z [rad]")
plt.savefig("2plot1D.png",bbox_inches='tight' )
plt.close()

# Figure 4
plt.figure(figsize=(8,8))
aux_th = np.arange(0,1000.)*2*np.pi/1000.
plt.plot(np.cos(aux_th), np.sin(aux_th), 'k-', alpha=0.25)
plt.plot(np.cos(theta), np.sin(theta), 'b<', alpha=0.75, ms=16)
plt.plot(np.cos(theta_mean), np.sin(theta_mean), 'go', alpha=1.0, ms=16, label=r"$(\cos(\overline{\theta}),\sin(\overline{\theta}))$") 
plt.plot(cos_mean/r, sin_mean/r, 'rs', alpha=1.0, ms=16, label=r"$(\overline{\cos(\theta}),\overline{\sin(\theta}))$") 
plt.grid('on')
plt.xlim([-1.1, 1.1])
plt.ylim([-1.1, 1.1])
plt.legend(loc="lower right", numpoints=1)
plt.xlabel(r"$x=cos(\theta)$ [$L$]")
plt.ylabel(r"$x=sin(\theta)$ [$L$]")
plt.savefig("3plot2D.png", bbox_inches='tight')
plt.close()


