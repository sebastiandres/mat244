import numpy as np 
from matplotlib import pyplot as plt
from scipy import stats

# Changing some default behavior
plt.rcParams['legend.numpoints'] = 1

data = np.loadtxt("cherry.txt", skiprows=2)
diam = data[:,0]
height = data[:,1]
volume = data[:,2]

plt.figure(figsize=(10,8))
plt.subplot(1,2,1)
plt.plot(diam, volume, 'ow', ms=8., mew=2.0)
plt.xlabel(r"Diametro $h$ [$in$]", fontsize=20)
plt.ylabel(r"Volumen $v$ [$ft^3$]", fontsize=20)
plt.xlim([0, 1.1*max(diam)])
plt.ylim([0, 1.1*max(volume)])
plt.subplot(1,2,2)
plt.plot(height, volume, 'sw', ms=8., mew=2.0)
plt.xlabel(r"Altura $h$ [$ft$]", fontsize=20)
plt.ylabel(r"Volumen $v$ [$ft^3$]", fontsize=20)
plt.xlim([0, 1.1*max(height)])
plt.ylim([0, 1.1*max(volume)])
plt.savefig("cherry1.png")
#plt.show()
plt.close()

# Linear figure
plt.figure(figsize=(10,8))
Pi2 = diam/height
Pi1 = volume/height**3
m, b, r, p_value, std_err = stats.linregress(Pi2, Pi1)
print m, b, r
plt.plot(Pi2, Pi1, 'x', ms=8., mew=2.)
x = np.sort(Pi2)
plt.plot(x, m*x + b, 'r', label = r"$\Pi_2 = %.5f \Pi_1 %+.5f$, $r^2=%.3f$" %(m,b,r))
plt.xlabel(r"$\Pi_2 = d / h$", fontsize=20)
plt.ylabel(r"$\Pi_1 = v / h^3 $", fontsize=20)
plt.xlim([0, 1.1*max(Pi2)])
plt.ylim([0, 1.1*max(Pi1)])
plt.legend(loc="upper left", prop={'size':12})
plt.savefig("cherry2.png")
#plt.show()

# Most general relationship
plt.figure(figsize=(10,8))
logPi2 = np.log(Pi2)
logPi1 = np.log(Pi1)
m, b, r, p_value, std_err = stats.linregress(logPi2, logPi1)
print m, b, r
plt.figure(figsize=(10,8))
plt.subplot(1,2,1)
plt.plot(logPi2, logPi1, 'x', ms=8., mew=2.)
x = np.sort(logPi2)
plt.plot(x, m*x + b, 'r', label = r"$\log(\Pi_2) = %.2f \log(\Pi_1) %+.2f$, $r^2=%.3f$" %(m,b,r))
plt.xlabel(r"$log(\Pi_2)$", fontsize=20)
plt.ylabel(r"$log(\Pi_1) $", fontsize=20)
plt.legend(loc="lower left", prop={'size':12})
plt.subplot(1,2,2)
plt.plot(Pi2, Pi1, 'x', ms=8., mew=2.)
x = np.sort(Pi2)
plt.plot(x, np.exp(b)*(x**m), 'r', label = r"$\Pi_2 = %.3f \Pi_1^{%+.2f}$" %(np.exp(b), m))
plt.legend(loc="upper left", prop={'size':12})
plt.savefig("cherry3.png")
#plt.show()
