#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Standard libraries
from matplotlib import pyplot as plt
import numpy as np

# Local files and libraries
import latex

# Debugging
from pdb import set_trace as st

#############################################################################
# Priorization
data1 = []

#############################################################################
# Priorization
#############################################################################
data1 = []
############################
# Wrong plot
############################
# The plot
fig = plt.figure()
plt.plot([1,2],[2,1])
plt.savefig("priorizacion_wrong.png", bbox_inches='tight')
############################
# Right plot - grab the data
############################
# Just the same data
plt.savefig("priorizacion_right.png", bbox_inches='tight')

#############################################################################
# Expresividad
#############################################################################
data2 = {"Apple":"EEUU", 
         "Lenovo":"Hong Kong",
         "HP":"EEUU",
         "Sony":"Japon",
         "Asus":"Taiwan",
         "Dell":"EEUU",
         "Samsung":"Corea del Sur",
         "Acer":"Taiwan",
         "MSI":"Taiwan",
         "Toshiba":"Japon"}

latex.texttable2tex(np.array([[k,v] for k,v in data2.items()]))

############################
# Wrong plot - grab the data
############################
make = data2.keys()
countries = data2.values()
unique_countries = list(set(countries))
y = np.arange(len(make))+0.5
x = [1+unique_countries.index(c) for c in countries]
# The plot
fig = plt.figure()
plt.barh(y, x, align="center", color='r', alpha=0.5)
plt.xticks(x, countries)
plt.yticks(y, make)
plt.gca().spines['right'].set_visible(False)
plt.gca().spines['top'].set_visible(False)
plt.savefig("expresividad_wrong.png", bbox_inches='tight')
############################
# Wrong plot - grab the data
############################
# Just the same data

#############################################################################
# Consistencia
#############################################################################
data3 =  []
# The plot
fig = plt.figure()
plt.plot([1,2],[2,1])
plt.savefig("consistencia_wrong.png", bbox_inches='tight')
############################
# Right plot - grab the data
############################
# Just the same data
plt.savefig("consistencia_right.png", bbox_inches='tight')
