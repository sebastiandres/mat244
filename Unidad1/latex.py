####################################################################################################
# CODE FOR EASY COPY-PASTING DATA INTO LATEX
####################################################################################################
def float2tex(my_float, scientific_notation=False, decimals=3):
  """ Turns "1.4E4" into "1.4\ \cdit 10^{4}" """
  if scientific_notation:
    my_format = "%" + "1.%dE"%decimals
    string = my_format %my_float
    string = string.replace("E", "\\ \\cdot 10^{") + "}"
  else:
    my_format = "%" + "1.%df"%decimals
    string = my_format %my_float
  return string

def matrix2tex(A, scientific_notation=False, decimals=2):
  """ Print out a matrix into latex code, to allow easier copy-paste into reports"""
  n_rows, n_cols = A.shape
  my_str = "\\begin{bmatrix} \n"
  for i in range(n_rows):
    stringed_values = [float2tex(value, scientific_notation, decimals) for value in A[i,:]]
    my_str += " & ".join(stringed_values) + "\\\\ \n"
  my_str += "\\end{bmatrix} \n"
  print my_str
  return my_str

def table2tex(A, scientific_notation=False, decimals=2):
  """ Print out a matrix into latex code, to allow easier copy-paste into reports"""
  n_rows, n_cols = A.shape
  alignment = "l|"*A.shape[1] if scientific_notation else "c|"*A.shape[1]
  my_str = "\\begin{center} \\scalebox{0.7}{\n  \\begin{tabular}"
  my_str+= "{|" + alignment+ "}"
  my_str+= " \hline \n"
  for i in range(n_rows):
    stringed_values = [float2tex(value, scientific_notation, decimals) for value in A[i,:]]
    my_str += "    " + " & ".join(stringed_values) + "\\\\ \\hline \n"
  my_str+= "  \\end{tabular}\n} \\end{center} \n"
  print my_str
  return my_str

def texttable2tex(A):
  """ Print out a matrix into latex code, to allow easier copy-paste into reports"""
  n_rows, n_cols = A.shape
  my_str = "\\begin{center} \\scalebox{0.7}{\n  \\begin{tabular}"
  my_str+= "{|" + "l|"*A.shape[1]+ "}"
  my_str+= " \hline \n"
  for i in range(n_rows):
    my_str += "    " + " & ".join(A[i,:]) + "\\\\ \\hline \n"
  my_str+= "  \\end{tabular}\n} \\end{center} \n"
  print my_str
  return my_str

if __name__=="__main__":
  import numpy as np
  A = np.round(10*np.random.rand(10,10),1)
  matrix2tex(A)  
