# Standard libraries
from matplotlib import pyplot as plt
import numpy as np
from scipy import stats

# Local files and libraries
import latex

# Debugging
from pdb import set_trace as st

data = np.array([
                [10.0, 	8.04,   10.0, 	9.14,  10.0, 	7.46, 	8.0, 	6.58],
                [8.0, 	6.95, 	8.0, 	8.14, 	8.0, 	6.77, 	8.0, 	5.76],
                [13.0, 	7.58,  13.0, 	8.74,  13.0,   12.74, 	8.0, 	7.71],
                [9.0, 	8.81, 	9.0, 	8.77, 	9.0, 	7.11, 	8.0, 	8.84],
                [11.0, 	8.33,  11.0, 	9.26,  11.0, 	7.81, 	8.0, 	8.47],
                [14.0, 	9.96,  14.0, 	8.10,  14.0, 	8.84, 	8.0, 	7.04],
                [6.0, 	7.24, 	6.0, 	6.13, 	6.0, 	6.08, 	8.0, 	5.25],
                [4.0, 	4.26, 	4.0, 	3.10, 	4.0, 	5.39, 	19.0,  12.50],
                [12.0, 10.83,  12.0, 	9.13,  12.0, 	8.15, 	8.0, 	5.56],
                [7.0, 	4.82,	7.0, 	7.26, 	7.0, 	6.42, 	8.0, 	7.91],
                [5.0, 	5.68, 	5.0, 	4.74, 	5.0, 	5.73, 	8.0, 	6.89]
                ])

set_1 = data[:,0:2]
set_2 = data[:,2:4]
set_3 = data[:,4:6]
set_4 = data[:,6:8]

# Print info
latex.table2tex(data)

# Compute the means and print them
print "Mean: " + "%.2f & "*8 %tuple(data.mean(axis=0))
print "Standard Deviation: " + "%.2f & "*8 %tuple(data.std(axis=0))

plot_line = True #False

def my_plot(ax, set, title):
  x, y = set[:,0],set[:,1]
  xmin, xmax = 0, 20
  ax.plot(x, y, 'go', alpha=1.0, zorder=10)
  ax.set_xlim([xmin, xmax])
  ax.set_ylim([xmin, xmax])
  ax.set_title(title)
  if plot_line:
    z = np.array([xmin, xmax])
    m, b, r, p_value, std_err = stats.linregress(x, y)
    print 
    #m, b = np.polyfit(x, y, 1)
    legend = r"$y=%.1f x + %.1f$, $R^2=%.2f$" %(m, b, r)
    print legend
    ax.plot(z, m*z + b, 'r-', lw=2.0, alpha=0.5, 
            label=legend, zorder=1)
    ax.legend(fontsize=12)
  return

# plot
fig = plt.figure(figsize=(8,8))
my_plot(plt.subplot(2,2,1), set_1, "I")
my_plot(plt.subplot(2,2,2), set_2, "II")
my_plot(plt.subplot(2,2,3), set_3, "III")
my_plot(plt.subplot(2,2,4), set_4, "IV")
plt.savefig("AnscombeQuartet%d.png" %plot_line, bbox_inches='tight')
#plt.show()
