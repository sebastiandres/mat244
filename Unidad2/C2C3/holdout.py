import model
from scipy import random

data = model.load_data("dataN20.txt")
N = data.shape[0]
split = int(0.7*N)

# Permute the data
my_seed = 27
random.seed(my_seed)
data = random.permutation(data)

# Do the split
training_data = data[:split,:]
prediction_data = data[split:,:]

# Train model excluding the holdout set
model_params = model.get_params(training_data)

# Test with the holdout set
prediction_error = model.get_error(model_params, prediction_data)

# Train model with all the data
new_model_params = model.get_params(data)

# Report
print "The old model parameters are"
print "(a,b,c,d) = (%.2f, %.2f, %.2f, %.2f)" %tuple(model_params)
print "The new model parameters are"
print "(a,b,c,d) = (%.2f, %.2f, %.2f, %.2f)" %tuple(new_model_params)
print "Conservative error estimation on prediction data: %.2f" %prediction_error
true_error = model.get_error(model.secret_true_params, prediction_data)
print "True error estimation on prediction data: %.2f" %true_error
all_error = model.get_error(model.secret_true_params, data)
print "True error estimation on all data: %.2f" %all_error

# Plot the model
model.plot(new_model_params, data)
