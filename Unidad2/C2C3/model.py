import numpy as np
from matplotlib import pyplot as plt
from scipy import optimize
from scipy import random 

secret_true_params = (5., np.pi/4.0, 0., 0.)
my_seed = 42
random.seed(my_seed)

def my_model(x, a, b, c, d):
  return a*np.cos(b*x+c) + d

def generate_data(N=100, true_params=secret_true_params, 
                  seed = 42):
  x = np.linspace(-2.5, 2.5, N)
  y1 = my_model(x, *true_params)
  y2 = 1.0 * random.normal(size=N)
  # Create the data
  data = np.array([x,y1+y2]).T
  # Shuffle the data
  permuted_data = random.permutation(data)
  # Save the data
  np.savetxt("dataN%d.txt"%N, data)
  return data

def load_data(myfile):
  data = np.loadtxt(myfile)
  return data

def get_params(data):
  # Use optimize to get A and B using the data
  xdata = data[:,0]
  ydata = data[:,1]
  popt, pcov = optimize.curve_fit(my_model, xdata, ydata, maxfev=2000)
  return popt

def get_error(model_params, data):
  x_data = data[:,0]
  y_data = data[:,1]
  y_prediction = my_model(x_data, *model_params)
  error = np.sum((y_data-y_prediction)**2)/len(y_data)
  return error

def plot(model_params, data, true_params=secret_true_params):
  plt.plot(data[:,0], data[:,1], 's', label="data", alpha=0.5)
  x = np.array(sorted(data[:,0].copy()))
  plt.plot(x, my_model(x, *true_params), 
           'g', label="true params", lw=2.0)
  plt.plot(x, my_model(x, *model_params), 
           'r', label="model params", lw=2.0)
  xmin, xmax = x.min(), x.max()
  plt.xlim([xmin-.2, xmax+0.2])
  plt.legend(numpoints=1, loc="lower center")
  plt.show()
  return

if __name__=="__main__":
  data = generate_data(N=42)
  plot(secret_true_params, data)
