import model

from scipy import random
import numpy as np

data = model.load_data("dataN50.txt")
N = data.shape[0]

# Permute the data
my_seed = 1
random.seed(my_seed)

# Parameters of Cross Validation
split = int(0.7*N)
Nreps = 2*split

# Perform the random cross validation
prediction_error = []
for i in range(Nreps):
  # Do the split
  index = random.permutation(range(N))
  training_data = data[index[:split],:]
  prediction_data = data[index[split:],:]
  # Train model excluding the holdout set
  model_params = model.get_params(training_data)
  # Test with the holdout set
  pe = model.get_error(model_params, prediction_data)
  prediction_error.append(pe)

# Train model with all the data
new_model_params = model.get_params(data)

# Compute mean and average prediction errors
prediction_error = np.array(prediction_error)

# Report
print "The new model parameters are"
print "(a,b,c,d) = (%.2f, %.2f, %.2f, %.2f)" %tuple(new_model_params)
print "Conservative error estimation on prediction data"
print "mean: %.2f" %prediction_error.mean()
print "std: %.2f" %prediction_error.std()
all_error = model.get_error(model.secret_true_params, data)
print "True error estimation on all data: %.2f" %all_error

# plot the model
model.plot(new_model_params, data)
