import model

from scipy import random
import numpy as np

data = model.load_data("dataN50.txt")
N = data.shape[0]

# Permute the data
my_seed = 1
random.seed(my_seed)

# Parameters of Cross Validation
ncv = 10
split = int((ncv-1.)*N/ncv)
split_size = N - split 
data = random.permutation(data)

# Perform the random cross validation
prediction_error = []
index = range(N)
for i in range(ncv):
  # Do the split
  start = i*(split_size)
  end = min((i+1)*split_size, N)
  training_index = index[:start]+index[end:]
  prediction_index = index[start:end]
  training_data = data[training_index,:]
  prediction_data = data[prediction_index,:]
  # Train model excluding the holdout set
  model_params = model.get_params(training_data)
  # Test with the holdout set
  pe = model.get_error(model_params, prediction_data)
  prediction_error.append(pe)

# Train model with all the data
new_model_params = model.get_params(data)

# Compute mean and average prediction errors
prediction_error = np.array(prediction_error)

# Report
print "The new model parameters are"
print "(a,b,c,d) = (%.2f, %.2f, %.2f, %.2f)" %tuple(new_model_params)
print "Conservative error estimation on prediction data"
print "mean: %.2f" %prediction_error.mean()
print "std: %.2f" %prediction_error.std()
all_error = model.get_error(model.secret_true_params, data)
print "True error estimation on all data: %.2f" %all_error

# plot the model
model.plot(new_model_params, data)
