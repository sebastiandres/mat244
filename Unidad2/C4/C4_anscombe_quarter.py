# Standard libraries
from matplotlib import pyplot as plt
import numpy as np
from scipy import optimize

# Debugging
from pdb import set_trace as st

data = np.array([
                [10.0, 	8.04,   10.0, 	9.14,  10.0, 	7.46, 	8.0, 	6.58],
                [8.0, 	6.95, 	8.0, 	8.14, 	8.0, 	6.77, 	8.0, 	5.76],
                [13.0, 	7.58,  13.0, 	8.74,  13.0,   12.74, 	8.0, 	7.71],
                [9.0, 	8.81, 	9.0, 	8.77, 	9.0, 	7.11, 	8.0, 	8.84],
                [11.0, 	8.33,  11.0, 	9.26,  11.0, 	7.81, 	8.0, 	8.47],
                [14.0, 	9.96,  14.0, 	8.10,  14.0, 	8.84, 	8.0, 	7.04],
                [6.0, 	7.24, 	6.0, 	6.13, 	6.0, 	6.08, 	8.0, 	5.25],
                [4.0, 	4.26, 	4.0, 	3.10, 	4.0, 	5.39, 	19.0,  12.50],
                [12.0, 10.83,  12.0, 	9.13,  12.0, 	8.15, 	8.0, 	5.56],
                [7.0, 	4.82,	7.0, 	7.26, 	7.0, 	6.42, 	8.0, 	7.91],
                [5.0, 	5.68, 	5.0, 	4.74, 	5.0, 	5.73, 	8.0, 	6.89]
                ])

set_1 = data[:,0:2]
set_2 = data[:,2:4]
set_3 = data[:,4:6]
set_4 = data[:,6:8]

def get_norm2_params(x,y):
  def my_model(x, m, b):
    return m*x+b
  popt, pcov = optimize.curve_fit(my_model, x, y, maxfev=2000)
  return popt

def get_norm1_params(x, y, p0):
  def my_model(p):
    m,b = p
    return np.abs(y-m*x-b).sum()
  sol = optimize.minimize(my_model, p0)
  #print sol
  return sol["x"]

def get_norminf_params(x, y, p0):
  def my_model(p):
    m,b = p
    return np.max(np.abs(y-m*x-b))
  sol = optimize.minimize(my_model, p0)
  #print sol
  return sol["x"]

def my_plot(ax, my_set, title):
  x, y = my_set[:,0], my_set[:,1]
  xmin, xmax = 0, 20
  ax.plot(x, y, 'ko', alpha=1.0, ms=10, zorder=10)
  ax.set_xlim([xmin, xmax])
  ax.set_ylim([xmin, xmax])
  ax.set_title(title)
  z = np.array([xmin, xmax])
  # Get in norm 2  
  m2, b2 = get_norm2_params(x, y)
  legend = r"$y=%.2f x + %.2f$ using $||\cdot||_2$" %(m2, b2)
  ax.plot(z, m2*z + b2, 'r-', lw=3.0, alpha=0.75, 
          label=legend, zorder=1)
  # Get in norm 1
  m1, b1 = get_norm1_params(x, y, [0.8*m2,0.8*b2])
  legend = r"$y=%.2f x + %.2f$ using $||\cdot||_1$" %(m1, b1)
  ax.plot(z, m1*z + b1, 'b-', lw=3.0, alpha=0.75, 
          label=legend, zorder=1)
  # Get in norm inf
  minf, binf = get_norminf_params(x, y, [0.8*m2,0.8*b2])
  legend = r"$y=%.2f x + %.2f$ using $||\cdot||_{\infty}$" %(minf, binf)
  ax.plot(z, minf*z + binf, 'g-', lw=3.0, alpha=0.75, 
          label=legend, zorder=1)
  # Plot legend
  ax.legend(fontsize=16)
  return

# plot
fig = plt.figure(figsize=(8,8))
my_plot(plt.subplot(1,1,1), set_1, "I")
plt.savefig("AnscombeQuartetI.png", bbox_inches='tight')
plt.show()

fig = plt.figure(figsize=(8,8))
my_plot(plt.subplot(1,1,1), set_2, "II")
plt.savefig("AnscombeQuartetII.png", bbox_inches='tight')
plt.show()

fig = plt.figure(figsize=(8,8))
my_plot(plt.subplot(1,1,1), set_3, "III")
plt.savefig("AnscombeQuartetIII.png", bbox_inches='tight')
plt.show()

fig = plt.figure(figsize=(8,8))
my_plot(plt.subplot(1,1,1), set_4, "IV")
plt.savefig("AnscombeQuartetIV.png", bbox_inches='tight')
plt.show()
