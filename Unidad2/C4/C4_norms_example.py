from matplotlib import pyplot as plt
import numpy as np

def norm_1(x,y):
  return abs(x)+abs(y)

def norm_2(x,y):
  return (x)**2 + (y)**2

def norm_inf(x,y):
  return np.maximum(abs(x),abs(y))

def foobar(x,y):
  return (x+1.0)**2 + (y+1.00)**2

N = 101
x1 = np.linspace(-2.0, 2.0, N)
x2 = np.linspace(-2.0, 2.0, N)
X,Y = np.meshgrid(x1,x2)
F = foobar(X,Y)

# Plot
plt.figure(figsize=(8,8))
plt.axis("equal")
levels = [0.0001, 1.0, 3.0, 5.0, 5.8284, 8.0, 12.]
CSF = plt.contour(X, Y, foobar(X,Y), levels,
                  colors='k', label="$F(x_1,x_2)$")
plt.clabel(CSF, inline=1, fontsize=10)
plt.contour(X, Y, norm_inf(X,Y), [1], 
            linewidths=2, label="$||x||_1=1$")
plt.legend(loc="lower center")
plt.xlabel("$x_1$")
plt.ylabel("$x_2$")
plt.show()


