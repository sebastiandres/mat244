# MAT244 - Aplicaciones de la Matemática en la Ingeniería #

Sección de los [Proyectos](https://bitbucket.org/sebastiandres/mat244/wiki/proyectos) 

En la [wiki](https://bitbucket.org/sebastiandres/mat244/wiki) del curso podrán encontrar el programa del curso, además de las lecturas, las tareas y los proyectos.

En [tools](https://bitbucket.org/sebastiandres/mat244/wiki/tools) algunos tips y enlaces respecto a python, numpy, scipy y matplotlib.

En el [repositorio de código](https://bitbucket.org/sebastiandres/mat244/src) podrán encontrar los desarrollos numéricos de las distintas presentaciones.