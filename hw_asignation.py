from scipy import random
import sys

def get_random_seed():
  seed = random.randint(2014)
  return seed

def assign_pairs(names, seed):
  random.seed(seed)
  couples = random.permutation(names)
  print "The following couples have been generated with seed %d" %seed
  max_couples = len(couples)/2
  for i in range(max_couples):
    if i<max_couples-1:
      print "\t%d : (%s, %s)" %(i+1, couples[2*i], couples[2*i+1])
    else:
      print "\t%d : (%s) " %(i+1, ", ".join(couples[2*i:]))
  return
   

if __name__=="__main__":
  names = ["F. ALFARO", "S. CONTRERAS", "D. CONTRERAS", "D. GAJARDO", "E. NUNHEZ", "A. OGUEDA",
           "J. PEREZ", "J. POBLETE", "G. RENSONNET", "A. SANDOVAL", "G. SOLIS", "A. TAPIA"]
  names = [name.title() for name in names]
  try:
    seed = int(sys.argv[1])
    print "Using given seed : %s" %seed
    assign_pairs(names, seed)      
  except:
    seed = get_random_seed()
    print "Using generated seed : %s" %seed
    assign_pairs(names, seed)      
